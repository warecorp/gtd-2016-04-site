<?php

/**
 * Implements hook_preprocess_HOOK().
 */
function training_day_preprocess_page(&$vars) {
  if (arg(1) == 3) {
    dsm($vars);
  }

  if (!empty($vars['is_front'])) {
    watchdog('training_day', 'It\'s a front page! Time is @time.', array('@time' => format_date(REQUEST_TIME)), WATCHDOG_DEBUG);
  }
}

/**
 * Implements hook_comment_view_alter().
 */
function training_day_comment_view_alter(&$build) {
  if (isset($build['links']['comment']['#links']['comment-reply'])) {
    unset($build['links']['comment']['#links']['comment-reply']);
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function training_day_form_comment_form_alter(&$form, &$form_state, $form_id) {
  if (isset($form['author']['name'])) {
    $form['author']['name']['#title_display'] = 'invisible';
    $form['author']['name']['#attributes']['placeholder'] = t('Name');
  }

  $form['comment_body'][LANGUAGE_NONE][0]['#title_display'] = 'invisible';
  $form['comment_body'][LANGUAGE_NONE][0]['#attributes']['placeholder'] = t('Comment');
  $form['comment_body'][LANGUAGE_NONE][0]['#resizable'] = FALSE;
  $form['comment_body']['#after_build'][] = '_theme_customize_comment_node_beat_form';
}

/**
 * Implements hook_preprocess_HOOK().
 */
function training_day_preprocess_node(&$vars) {
  if (!empty($vars['page'])) {
    $user = user_load($vars['uid']);
    $vars['authorship'] = theme_html_tag(array(
      'element' => array(
        '#tag' => 'div',
        '#value' => t('This article was added by %name.', array('%name' => $user->field_first_name[LANGUAGE_NONE][0]['value'])),
        '#attributes' => array(
          'class' => array('messages', 'warning'),
        ),
      ),
    ));
  }
}

/**
 * Overrides theme_pager().
 *
 * @see theme_pager()
 */
function training_day_pager($variables) {
  $tags = $variables['tags'];
  $element = $variables['element'];
  $parameters = $variables['parameters'];
  $quantity = $variables['quantity'];
  global $pager_page_array, $pager_total;

  // Calculate various markers within this pager piece:
  // Middle is used to "center" pages around the current page.
  $pager_middle = ceil($quantity / 2);
  // current is the page we are currently paged to
  $pager_current = $pager_page_array[$element] + 1;
  // first is the first page listed by this pager piece (re quantity)
  $pager_first = $pager_current - $pager_middle + 1;
  // last is the last page listed by this pager piece (re quantity)
  $pager_last = $pager_current + $quantity - $pager_middle;
  // max is the maximum page number
  $pager_max = $pager_total[$element];
  // End of marker calculations.

  // Prepare for generation loop.
  $i = $pager_first;
  if ($pager_last > $pager_max) {
    // Adjust "center" if at end of query.
    $i = $i + ($pager_max - $pager_last);
    $pager_last = $pager_max;
  }
  if ($i <= 0) {
    // Adjust "center" if at start of query.
    $pager_last = $pager_last + (1 - $i);
    $i = 1;
  }
  // End of generation loop preparation.

  $li_first = theme('pager_first', array('text' => (isset($tags[0]) ? $tags[0] : t('� first')), 'element' => $element, 'parameters' => $parameters));
  $li_previous = theme('pager_previous', array('text' => (isset($tags[1]) ? $tags[1] : t('� previous')), 'element' => $element, 'interval' => 1, 'parameters' => $parameters));
  $li_next = theme('pager_next', array('text' => (isset($tags[3]) ? $tags[3] : t('next �')), 'element' => $element, 'interval' => 1, 'parameters' => $parameters));
  $li_last = theme('pager_last', array('text' => (isset($tags[4]) ? $tags[4] : t('last �')), 'element' => $element, 'parameters' => $parameters));

  if ($pager_total[$element] > 1) {
    if ($li_first) {
      $items[] = array(
        'class' => array('pager-first'),
        'data' => $li_first,
      );
    }
    if ($li_previous) {
      $items[] = array(
        'class' => array('pager-previous'),
        'data' => $li_previous,
      );
    }

    // When there is more than one page, create the pager list.
    if ($i != $pager_max) {
      if ($i > 1) {
        $items[] = array(
          'class' => array('pager-ellipsis'),
          'data' => '�',
        );
      }
      // Now generate the actual pager piece.
      for (; $i <= $pager_last && $i <= $pager_max; $i++) {
        if ($i < $pager_current) {
          $items[] = array(
            'class' => 'pager-item',
            'data' => theme('pager_previous', array('text' => $i, 'element' => $element, 'interval' => ($pager_current - $i), 'parameters' => $parameters)),
          );
        }
        if ($i == $pager_current) {
          $items[] = array(
            'class' => 'pager-current',
            'data' => $i,
          );
        }
        if ($i > $pager_current) {
          $items[] = array(
            'class' => 'pager-item',
            'data' => theme('pager_next', array('text' => $i, 'element' => $element, 'interval' => ($i - $pager_current), 'parameters' => $parameters)),
          );
        }
      }
      if ($i < $pager_max) {
        $items[] = array(
          'class' => array('pager-ellipsis'),
          'data' => '�',
        );
      }
    }
    // End generation.
    if ($li_next) {
      $items[] = array(
        'class' => array('pager-next'),
        'data' => $li_next,
      );
    }
    if ($li_last) {
      $items[] = array(
        'class' => array('pager-last'),
        'data' => $li_last,
      );
    }
    return theme('item_list', array(
      'items' => $items,
      'attributes' => array('class' => array('pager')),
    ));
  }
}

/**
 * Removes a text format selection for comment's body.
 */
function _theme_customize_comment_node_beat_form(&$form) {
  $form[LANGUAGE_NONE][0]['format']['#access'] = FALSE;
  return $form;
}
